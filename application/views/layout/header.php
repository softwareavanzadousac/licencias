<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Renap | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/assets/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="/assets/plugins/daterangepicker/daterangepicker.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<!-- jQuery -->
	<script src="/assets/plugins/jquery/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button)

	</script>
	<!-- Bootstrap 4 -->
	<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- daterangepicker -->
	<script src="/assets/plugins/moment/moment.min.js"></script>
	<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="/assets/dist/js/adminlte.js"></script>
	<!-- DataTables -->
	<script src="/assets/plugins/datatables/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
				</li>
			</ul>

			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<li class="nav-item d-none d-sm-inline-block">
					<a href="#" class="nav-link">Cerrar Sesión</a>
				</li>
			</ul>
		</nav>
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Brand Logo -->
			<a href="index3.html" class="brand-link">
				<span class="brand-text font-weight-light">Renap</span>
			</a>

			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
						data-accordion="false">
						<li class="nav-item">
							<a href="<?php echo base_url('interno/personas')?>" class="nav-link">
								<i class="nav-icon fas fa-user"></i>
								<p>Personas</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('interno/nacimiento')?>" class="nav-link">
								<i class="nav-icon fas fa-user"></i>
								<p>Nacimientos</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('interno/defuncion')?>" class="nav-link">
								<i class="nav-icon fas fa-skull-crossbones"></i>
								<p>Defunciones</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('interno/inicio')?>" class="nav-link">
								<i class="nav-icon fas fa-venus-mars"></i>
								<p>Casamientos</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('interno/inicio')?>" class="nav-link">
								<i class="fas fa-venus"></i>
								<i class="fas fa-mars"></i>
								<p>Divorcios</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('interno/inicio')?>" class="nav-link">
								<i class="nav-icon fas fa-id-card"></i>
								<p>DPI</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('interno/inicio')?>" class="nav-link">
								<i class="nav-icon fas fa-address-card"></i>
								<p>Licencias</p>
							</a>
						</li>
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
		<?php $success      = $this->session->flashdata('success_msg'); ?>
		<?php if($success): ?>
			<div class="alert alert-success" role="alert"><?php echo $success; ?></div>
		<?php endif; ?>
		<?php $error      = $this->session->flashdata('error_msg'); ?>
		<?php if($error): ?>
			<div class="alert alert-error" role="alert"><?php echo $error; ?></div>
		<?php endif; ?>
