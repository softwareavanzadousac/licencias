<?php
class Licencia_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
	}
	
	public function checkLogin($dpi, $password)
	{
		$existe = $this->db->from('Persona')->where('dpi',$dpi)->count_all_results();
		if($existe == 0){
			return 52;
		}
		$vivo = $this->db->from('Persona')->where('dpi',$dpi)->where('difunto',0)->count_all_results();
		if($vivo == 0){
			return 62;
		}
		$existe2 = $this->db->from('Licencia')->where('dpi',$dpi)->count_all_results();
		if($existe2 > 0){
			return 0;
		}
		$this->db->insert('Licencia', $password);
		return 1;
	}

	public function obtenerDatos($dpi)
	{
		$existe = $this->db->from('Licencia')->where('dpi',$dpi)->count_all_results();
		if($existe == 0){
			return new stdClass();
		}
		//return $this->db->select('anios, tipo')->from('Licencia')->where('dpi',$dpi)->get()->row();
		
		return $this->db->from('Licencia l')
						->select("p.apellidos apellidos, p.nombres nombres, p.fecha_nacimiento fecha, l.anios anios, l.tipo tipo",false)
						->join('Persona p','p.dpi = l.dpi AND l.dpi = '.$dpi)
						->get()->row();
	}

	public function editDatos($dpi, $password)
	{
		$this->db->where('dpi', $dpi);
		return $this->db->update('Licencia', $password);
	}
}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
