<?php
/**
 * Controller maestro que establece login al ser extendido por algun controlador en la carpeta "controllers"
 *
 * @author Guido Orellana <guido@grupoperinola.com>
 * @since 8 nov 2012
*/

class MY_Controller extends CI_Controller {

    private $userdata;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE) {
    		redirect('login');
        }
        $this->userdata = $this->session->userdata;
	}
}
//require(APPPATH.'libraries/PL_Controller.php');
