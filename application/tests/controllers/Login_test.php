<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_test extends TestCase {

	function __construct() {
		parent::__construct();
        log_message('debug', 'Unit_tests Controller Initialized');
	}
	protected $strictRequestErrorCheck = false;

	public function test_login_post()
	{
		$dpi = '1621991710101';
		$password = 'abc123';
		try {
			$output = $this->request(
				'POST', 'api/login', ['dpi' => $dpi, 'password' => $password]
			);
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		$this->assertResponseCode(200);
	}
}
