<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/JWT.php';
use \Firebase\JWT\JWT;
class Licencia extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->load->model('licencia_model','licencia');
	}

	public function index_post()
	{   
		$_POST = json_decode(file_get_contents("php://input"), true);
		$dpi = (int) $this->input->post('dpi');
		$data = array(
			'dpi' => $dpi,
			'tipo' => $this->input->post('tipo'),
			'anios' => 0
		);
        $persona = $this->licencia->checkLogin($dpi,$data);

        if($persona == 52){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'No existe el DPI '.$dpi.' dentro del sistema'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		elseif($persona == 62){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'El DPI '.$dpi.' ya fue dado de baja'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		elseif($persona == 0){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'El DPI '.$dpi.' ya tiene licencia'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		else{
    		$respuesta = array(
				'estado' => '200',
				'mensaje' => 'Creacion de licencia existosa'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
	}
}
