<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/JWT.php';
use \Firebase\JWT\JWT;
class Actualizar extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->load->model('licencia_model','licencia');
	}

	public function index_post()
	{   
		$_POST = json_decode(file_get_contents("php://input"), true);
		$dpi = (int) $this->input->post('dpi');
		$data = array(
			'tipo' => $this->input->post('tipo')
		);
		$persona = $this->licencia->editDatos($dpi,$data);
		if($persona == 1){
			$respuesta = array(
				'estado' => '200',
				'mensaje' => 'Transaccion existosa'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		else{
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'Transaccion incompleta'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		
	}

	public function obtener_post()
	{  
		$_POST = json_decode(file_get_contents("php://input"), true);
		$dpi = (int) $this->input->post('dpi');
		$persona = $this->licencia->obtenerDatos($dpi);
		if($persona  == new \stdClass()){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'No se encuentra la licencia ('.$dpi.')'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		else{
			$respuesta = array(
				'apellidos' => $persona->apellidos,
				'nombre' => $persona->nombres, 
				'tipo' => $persona->tipo, 
				'fechanac' => $persona->fecha,
				'anosantiguedad' => $persona->anios
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		

		/*$tipo_actual = $persona->tipo;
		$tipo_futuro = '';
		if($tipo_actual == 'b'){
			$tipo_futuro = 'a';
		}
		elseif($tipo_actual == 'c'){
			$tipo_futuro = 'b';
		}
		
		$data = array(
			'anios' => $persona->anios,
			'tipo_actual' => $tipo_actual,
			'tipo_futuro' => $tipo_futuro
		);
		$this->response($data, REST_Controller::HTTP_OK);*/
	}
}
